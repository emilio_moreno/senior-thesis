import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import random

from cvxpy import *

'''
Monthly data downloaded from https://finance.yahoo.com/quote/%5EGSPC/history?p=%5EGSPC
'''

df = pd.read_csv('/Users/emiliomoreno/Downloads/SPY_data.csv') # S&P 500 monthly data	
df['Date'] = pd.to_datetime(df['Date'])  # convert strings to datetime objects

df['returns'] = df['Open'].pct_change(1) # 1 for one month lookback
df['returns'].iloc[0] = 0                # Assume first day return is 0

# Trend-filtering algorithm

x = df['returns'].values
l = .1                                   # trend filtering parameter

np.random.seed(1)
n = len(x)
B = Variable(n)
D = np.identity(n)
rng = np.arange(n-1)
D[rng, rng+1] = -1
D = D[0:n-1]

Z = sum_squares(x-B) +  l * norm(D*B,1)
obj = Minimize(Z)
prob = Problem(obj)

prob.solve()

y = np.sign(B.value)

# plot S&P index

start = []
end = []

for i in range(len(y)-1):
	if y[i+1] < y[i]:  # switch from growth to crash
		start.append(i+1)
	if y[i+1] > y[i]:  # switch from crash to growth
		end.append(i)

if y[len(y)-1] == -1:
	end.append(len(y)-1)


# plot index
plt.plot(df['Date'],df['Open'])

# plot vertical bands
for j in range(len(start)):
	plt.axvspan(df['Date'][start[j]], df['Date'][end[j]], alpha=0.5, color='red')

plt.title('Two Regimes for the S&P 500')

plt.show()