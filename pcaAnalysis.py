import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import random

import datetime

from cvxpy import *
import statsmodels.formula.api as sm

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA as sklearnPCA

# REIT data downlaoded from CRSP

xls = pd.ExcelFile('/Users/emiliomoreno/Desktop/SeniorThesis/ZimanMonthly.xlsx')
reits = pd.read_excel(xls)

xls = pd.ExcelFile('/Users/emiliomoreno/Desktop/SeniorThesis/MarketDataMonthly.xlsx')
market = pd.read_excel(xls)

xls = pd.ExcelFile('/Users/emiliomoreno/Desktop/SeniorThesis/FEDratesMonthly.xlsx')
interestRates = pd.read_excel(xls)

returns = pd.DataFrame()
returns['Calendar Date'] = market['Date of Observation']

# value weighted portfolios
returns['REIT'] = reits[reits['INDNO']==1000800]['Index Total Return'].values
returns['EREIT'] = reits[reits['INDNO']==1000801]['Index Total Return'].values
returns['MREIT'] = reits[reits['INDNO']==1000802]['Index Total Return'].values
returns['HREIT'] = reits[reits['INDNO']==1000803]['Index Total Return'].values

# market returns
returns['SPY'] = market['Return on the S&P 500 Index']
returns['NYSE'] = market['Value-Weighted Return-incl. dividends']
returns['ONE'] = interestRates['Treasury Constant Maturity 1-year']/100
returns['TEN'] = interestRates['Treasury Constant Maturity 10-year']/100
returns['Baa'] = interestRates['Moodys Baa']/100

returns['Normalized Date'] = [datetime.datetime.strptime(str(d),'%Y%m%d') for d in returns['Calendar Date']]
returns['Year'] = [d.year for d in returns['Normalized Date']]

# equally weighted portfolios
returns['REIT2'] = reits[reits['INDNO']==1000850]['Index Total Return'].values
returns['EREIT2'] = reits[reits['INDNO']==1000851]['Index Total Return'].values
returns['MREIT2'] = reits[reits['INDNO']==1000852]['Index Total Return'].values
returns['HREIT2'] = reits[reits['INDNO']==1000853]['Index Total Return'].values

def trendFiltering(index,startYear,endYear,l):

	# trend filtering routine adapted from Mulvey and Liu (2011)

	x = returns[index][(startYear <= returns['Year']) & (returns['Year'] <= endYear)].values
	np.random.seed(1)
	n = len(x)
	B = Variable(n)
	D = np.identity(n)
	rng = np.arange(n-1)
	D[rng, rng+1] = -1
	D = D[0:n-1]
	Z = sum_squares(x-B) +  l * norm(D*B,1)
	obj = Minimize(Z)
	prob = Problem(obj)
	prob.solve()
	y = np.sign(B.value)
	dfY = pd.DataFrame()
	dfY['Date'] = returns['Normalized Date'][(startYear <= returns['Year']) & (returns['Year'] <= endYear)].values
	dfY['y'] = y
	dfY['B'] = B.value
	return dfY


def PCA1(index,startYear=1980,endYear=2017,l=0.1):

	# Principal Component Analysis for REIT returns

	dfY = trendFiltering('SPY',startYear,endYear,l)
	ret = returns.merge(dfY,left_on='Normalized Date',right_on='Date')
	cols = [index,'SPY','ONE','TEN','Baa']
	X = ret.ix[:,cols].values
	y = ret.ix[:,'Year'].values
	regime = ret.ix[:,'y']
	X_std = StandardScaler().fit_transform(X)
	X_std = X
	mean_vec = np.mean(X_std, axis=0)
	cov_mat = np.cov(X_std.T)
	[eig_vals, eig_vecs] = np.linalg.eig(cov_mat)
	cor_mat1 = np.corrcoef(X_std.T)
	cor_mat2 = np.corrcoef(X.T)
	u,s,v = np.linalg.svd(X_std.T)
	eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]
	eig_pairs.sort()
	eig_pairs.reverse()
	tot = sum(eig_vals)
	var_exp = [(i / tot)*100 for i in sorted(eig_vals, reverse=True)]
	cum_var_exp = np.cumsum(var_exp)
	matrix_w = np.hstack((eig_pairs[0][1].reshape(X.shape[1],1), eig_pairs[1][1].reshape(X.shape[1],1)))
	Y = X_std.dot(matrix_w)
	return X, X_std, y, regime, var_exp, cum_var_exp, matrix_w, Y, cov_mat


# no standardizing of data:
def PCA2(index,startYear=1980,endYear=2017,l=0.1):
	dfY = trendFiltering('SPY',startYear,endYear,l)
	ret = returns.merge(dfY,left_on='Normalized Date',right_on='Date')
	cols = [index,'SPY','ONE','TEN','Baa']
	X = ret.ix[:,cols].values
	y = ret.ix[:,'Year'].values
	regime = ret.ix[:,'y']
	mean_vec = np.mean(X, axis=0)
	cov_mat = np.cov(X.T)
	[eig_vals, eig_vecs] = np.linalg.eig(cov_mat)
	u,s,v = np.linalg.svd(X.T)
	eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]
	eig_pairs.sort()
	eig_pairs.reverse()
	tot = sum(eig_vals)
	var_exp = [(i / tot)*100 for i in sorted(eig_vals, reverse=True)]
	cum_var_exp = np.cumsum(var_exp)
	matrix_w = np.hstack((eig_pairs[0][1].reshape(X.shape[1],1), eig_pairs[1][1].reshape(X.shape[1],1)))
	Y = X.dot(matrix_w)
	return X, y, regime, var_exp, cum_var_exp, matrix_w, Y, cov_mat


rolling_data = {}
window = {}

indexes = ['REIT','EREIT','MREIT','HREIT','REIT2','EREIT2','MREIT2','HREIT2']

for index in indexes:
	rd = []
	ww = []
	for i in range(1980,2013):
		[X, X_std, y, regime, var_exp,cum_var_exp,W, Y, cov_mat] = PCA1(index,i,i+5)
		rd.append(cum_var_exp[1])
		ww.append(i+5)
	rolling_data[index] = rd
	window[index] = ww

for i in ['REIT','EREIT','MREIT','HREIT']:
	plt.plot(window[i],rolling_data[i],label=i)


plt.legend()
plt.show()

def AR(X,W):

	# computation of Absorption Ratio

	n = 2
	num = 0
	for i in range(n):
		num += W.T[i].var()
	N = len(X.T)
	denom = 0
	for j in range(N):
		denom += X.T[j].var()
	return num, denom


##### BY YEAR ######
traces = []

for year in np.unique(y):
    trace = Scatter(
        x=Y[y==year,0],
        y=Y[y==year,1],
        mode='markers',
        name=year,
        marker=Marker(
            size=12,
            line=Line(
                color='rgba(217, 217, 217, 0.14)',
                width=0.5),
            opacity=0.8))
    traces.append(trace)


data = Data(traces)
layout = Layout(showlegend=True,
                scene=Scene(xaxis=XAxis(title='PC1'),
                yaxis=YAxis(title='PC2'),))

fig = Figure(data=data, layout=layout)
py.plot(fig)

##### BY YEAR 2######
traces = []

for year in np.unique(y):
    trace = Scatter(
        x=X[y==year,0],
        y=X[y==year,1],
        mode='markers',
        name=year,
        marker=Marker(
            size=12,
            line=Line(
                color='rgba(217, 217, 217, 0.14)',
                width=0.5),
            opacity=0.8))
    traces.append(trace)


data = Data(traces)
layout = Layout(showlegend=True,
                scene=Scene(xaxis=XAxis(title='PC1'),
                yaxis=YAxis(title='PC2'),))

fig = Figure(data=data, layout=layout)
py.plot(fig)


#### BY REGIME #######

traces = []

for r in [-1,1]:
    trace = Scatter(
        x=Y[regime==r,0],
        y=Y[regime==r,1],
        mode='markers',
        name=r,
        marker=Marker(
            size=12,
            line=Line(
                color='rgba(217, 217, 217, 0.14)',
                width=0.5),
            opacity=0.8))
    traces.append(trace)


data = Data(traces)
layout = Layout(showlegend=True,
                scene=Scene(xaxis=XAxis(title='PC1'),
                yaxis=YAxis(title='PC2'),))

fig = Figure(data=data, layout=layout)
py.plot(fig)


#### BY REGIME 2#######

traces = []

for r in [-1,1]:
    trace = Scatter(
        x=X[regime==r,0],
        y=X[regime==r,1],
        mode='markers',
        name=r,
        marker=Marker(
            size=12,
            line=Line(
                color='rgba(217, 217, 217, 0.14)',
                width=0.5),
            opacity=0.8))
    traces.append(trace)


data = Data(traces)
layout = Layout(showlegend=True,
                scene=Scene(xaxis=XAxis(title='PC1'),
                yaxis=YAxis(title='PC2'),))

fig = Figure(data=data, layout=layout)
py.plot(fig)


def PCA(index):
	dfY = trendFiltering('SPY',1980,2017,0.1)
	ret = returns.merge(dfY,left_on='Normalized Date',right_on='Date')
	cols = [index,'SPY','ONE','TEN','Baa']
	X = ret.ix[:,cols].values
	y = ret.ix[:,'Year'].values
	regime = ret.ix[:,'y']
	X_std = StandardScaler().fit_transform(X)
	sklearn_pca = sklearnPCA(n_components=2)
	Y_sklearn = sklearn_pca.fit_transform(X_std)
	return Y_sklearn

#### BY REGIME #######

Y = PCA('REIT')

traces = []

for r in [-1,1]:
    trace = Scatter(
        x=Y[regime==r,0],
        y=Y[regime==r,1],
        mode='markers',
        name=r,
        marker=Marker(
            size=12,
            line=Line(
                color='rgba(217, 217, 217, 0.14)',
                width=0.5),
            opacity=0.8))
    traces.append(trace)


data = Data(traces)
layout = Layout(showlegend=True,
                scene=Scene(xaxis=XAxis(title='PC1'),
                yaxis=YAxis(title='PC2'),))

fig = Figure(data=data, layout=layout)
py.plot(fig)

