Sample implementation of the following algorithms:

Trend filtering analysis: adapted from Mulvey and Liu's (2011, 2016) application to University endowment management (https://doi.org/10.3905/jpm.2016.43.1.100)

Principal Component Analysis for portfolio optimization: adapted from Yang's (2015) analysis of Australian equity behavior (https://ir.canterbury.ac.nz/bitstream/handle/10092/10293/thesis.pdf)